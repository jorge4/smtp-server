#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

    typedef bool (*smtp_auth_handler_function)(char *user, char *pass, void *userdata);
    typedef char *(*smtp_auth_cram_handler_function)(char *user, void *userdata);

    void set_auth_plain_login_handler(smtp_auth_handler_function handler, void *ud);
    void set_auth_cram_handler(smtp_auth_cram_handler_function handler, void *ud);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

#include <string>
#include <sstream>
#include <vector>

using namespace std;

typedef struct
{
    string user;
    string from;
    vector<string> to;
    string mail_data;
} smtp_mail;

typedef bool (*smtp_mail_handler_function)(smtp_mail *mail, char **error, void *userdata);
void set_mail_handler(smtp_mail_handler_function handler, void *ud);

#endif
