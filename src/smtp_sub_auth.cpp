#include "smtp_server_internal.h"

//char password[] = "$password$";

string base64_decode_mbed(char *str)
{
    size_t olen;
    char autsch[512];
    memset(autsch, '\0', 512);
    mbedtls_base64_decode((unsigned char *)autsch, 512, &olen, (unsigned char *)str, strlen(str));
    string auths(autsch);
    return auths;
}

void hmac_md5_mbed(char *dest, char *key, char *payload)
{
    uint8_t hmacResult[32];

    mbedtls_md_context_t ctx;
    mbedtls_md_type_t md_type = MBEDTLS_MD_MD5;

    const size_t payloadLength = strlen(payload);
    const size_t keyLength = strlen(key);

    mbedtls_md_init(&ctx);
    mbedtls_md_setup(&ctx, mbedtls_md_info_from_type(md_type), 1);
    mbedtls_md_hmac_starts(&ctx, (const unsigned char *)key, keyLength);
    mbedtls_md_hmac_update(&ctx, (const unsigned char *)payload, payloadLength);
    mbedtls_md_hmac_finish(&ctx, hmacResult);
    mbedtls_md_free(&ctx);

    stringstream res;
    for (int i = 0; i < sizeof(hmacResult); i++)
    {
        char str[3];
        sprintf(str, "%02x", (int)hmacResult[i]);
        res << str;
    }
    string ress = res.str();
    strcpy(dest, ress.c_str());
    dest[32 * 2 + 1] = '\0';
}

bool auth_process(struct mg_connection *nc, char *line, char *line_cased)
{
    mail_state *state = (mail_state *)nc->user_data;
    if (state->waiting_cram_response)
    {
        string auths = base64_decode_mbed(line);
        int space = auths.find(' ');
        string user = auths.substr(0, space);
        state->user = strdup(user.c_str());
        string hash = auths.substr(space + 1, auths.length());
        char cram_digest_calcd[400];
        memset(cram_digest_calcd, '\0', 400);
        state->waiting_cram_response = false;
        // TODO consultar user antes de calcular hmac
        char * pass = auth_cram_handler(state->user, auth_cram_ud);
        hmac_md5_mbed(cram_digest_calcd, pass, state->cram_challenge);
        free(pass);
        // LOG(LL_INFO, (">>>>>>>>>>>>>>>>>  AUTH CRAM data: < %s : %s : %s >", line, auths.c_str(), cram_digest_calcd));
        // LOG(LL_INFO, (">>>>>>>>>>>>>>>>>  AUTH CRAM data: < %s : %s >", user.c_str(), hash.c_str()));
        if (strncmp(hash.c_str(), cram_digest_calcd, hash.length()) == 0)
        {
            free(state->cram_challenge);
            state->cram_challenge = NULL;
            state->autenticated = true;
            mg_printf(nc, "%s", "235 2.7.0 Authentication successful\n");
        }
        else
        {
            mg_printf(nc, "%s", "535 5.7.8  Authentication credentials invalid\n");
        }
        return true;
    }
    else if (state->waiting_plain)
    {
        string auths = base64_decode_mbed(line);
        char data[auths.length() + 1];
        memcpy(data, auths.c_str(), auths.length());
        data[auths.length()] = '\0';
        state->user = strdup(data + 1);
        char *pass = strdup(data + 1 + strlen(state->user) + 1);
        // LOG(LL_INFO, (">>>>>>>>>>>>>>>>>  AUTH PLAIN data: < %s : %s >", state->user, pass));
        state->waiting_plain = false;
        bool authed = auth_handler(state->user, pass, auth_ud);
        free(pass);
        if (authed)
        {
            mg_printf(nc, "%s", "235 2.7.0 Authentication successful\n");
            state->autenticated = true;
        }
        else
        {
            mg_printf(nc, "%s", "535 5.7.8  Authentication credentials invalid\n");
        }
        return true;
    }
    else if (state->waiting_login_pass)
    {
        string auths = base64_decode_mbed(line); // pass
        // LOG(LL_INFO, (">>>>>>>>>>>>>>>>>  AUTH LOGIN pass: <%s>", auths.c_str()));
        state->waiting_login_pass = false;
        bool authed = auth_handler(state->user, (char*)auths.c_str(), auth_ud);
        if (authed)
        {
            mg_printf(nc, "%s", "235 2.7.0 Authentication successful\n");
            state->autenticated = true;
        }
        else
        {
            mg_printf(nc, "%s", "535 5.7.8  Authentication credentials invalid\n");
        }
        return true;
    }
    else if (state->waiting_login_user)
    {
        string auths = base64_decode_mbed(line);
        // LOG(LL_INFO, (">>>>>>>>>>>>>>>>>  AUTH LOGIN user: <%s>", auths.c_str()));
        state->user = strdup(auths.c_str());
        mg_printf(nc, "%s", "334 UGFzc3dvcmQ6\n");
        state->waiting_login_pass = true;
        state->waiting_login_user = false;
        return true;
    }
    else if (strncmp(line_cased, "AUTH CRAM-MD5", 13) == 0 && SMTP_AUTH_CRAM_IS_ENABLED())
    {

        state->waiting_cram_response = true;
        char challenge[256];
        snprintf(challenge, 256, "<%12.9f-%ul@%s>", mgos_uptime(), esp_random(), "smtp server salt");
        state->cram_challenge = strdup(challenge);
        char challenge_encoded[256];
        size_t olen;
        mbedtls_base64_encode((unsigned char *)challenge_encoded, 256, &olen, (unsigned char *)challenge, strlen(challenge));
        mg_printf(nc, "%s%s\n", "334 ", challenge_encoded);
        // LOG(LL_INFO, (">>>>>>>>>>>>>>>>>  AUTH CRAM ch: <%s> chh: <%s>", challenge, challenge_encoded));
        return true;
    }
    else if (strncmp(line_cased, "AUTH LOGIN", 10) == 0 && SMTP_AUTH_LOGIN_IS_ENABLED())
    {
        char *rest = line + 11;
        bool has_info_already = strlen(line) > 11;
        state->waiting_login_user = true;
        if (has_info_already)
        {
            process_server_line(nc, rest);
        }
        else
        {
            mg_printf(nc, "%s", "334 VXNlcm5hbWU6\n");
        }
        return true;
    }
    else if (strncmp(line_cased, "AUTH PLAIN", 10) == 0 && SMTP_AUTH_PLAIN_IS_ENABLED())
    {
        char *rest = line + 11;
        bool has_info_already = strlen(line) > 11;
        state->waiting_plain = true;
        if (has_info_already)
        {
            process_server_line(nc, rest);
        }
        else
        {
            mg_printf(nc, "%s", "334 \n");
        }
        return true;
    }
    return false;
}