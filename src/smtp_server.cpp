#include "mgos.h"
#include "smtp_server_internal.h"

smtp_auth_handler_function auth_handler;
void * auth_ud;
smtp_auth_cram_handler_function auth_cram_handler;
void * auth_cram_ud;
smtp_mail_handler_function mail_handler;
void * mail_ud;


void set_auth_plain_login_handler(smtp_auth_handler_function handler, void * ud){
  auth_handler = handler;
  auth_ud = ud;
}

void set_auth_cram_handler(smtp_auth_cram_handler_function handler, void * ud){
  auth_cram_handler = handler;
  auth_cram_ud = ud;
}

void set_mail_handler(smtp_mail_handler_function handler, void * ud){
  mail_handler = handler;
  mail_ud = ud;
}

bool mgos_smtp_server_init(void) {
  setup_server();
  return true;
}
