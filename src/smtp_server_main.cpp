#include "mgos.h"
#include "mgos_mongoose.h"

// #include <string>
// #include <sstream>
// #include <bits/stdc++.h>
// // #include <base64.h>
// #include <mbedtls/md.h>
// #include <mbedtls/base64.h>

#include "smtp_server_internal.h"

//static unsigned long s_next_id = 0;

void strupp(char *beg)
{
    while ((*beg = std::toupper(*beg)))
        ++beg;
}

void clear_state(mail_state *state)
{
    if (state->to)
        state->to->clear();
    free(state->from);
    free(state->cram_challenge);
    free(state->user);
    if (state->mail_data)
        state->mail_data->clear();
    stringstream *s_save = state->s;
    memset(state, 0, sizeof(mail_state)); // todo NULL y falso;
    state->s = s_save;
}

void process_server_line(struct mg_connection *nc, char *line)
{
    char line_cased[1024];
    strncpy(line_cased, line, 1024);
    strupp(line_cased);
    mail_state *state = (mail_state *)nc->user_data;
    if (strlen(line) == 0 && !state->receiving_data)
        return;

    if (state->data_overflow && !(strlen(line) == 1 && line[0] == '.'))
    {
        // LOG(LL_INFO, ("### %lu free ## buff %d", (unsigned long)mgos_get_free_heap_size(), state->s ? state->s->str().length() : -1));
        return;
    }
    // LOG(LL_INFO, ("++++++++  linca <%s>", line));

    bool auth_processed = auth_process(nc, line, line_cased);

    if (auth_processed)
        return;

    bool send_mail_proccessed = send_mail_process(nc, line, line_cased);

    if (send_mail_proccessed)
        return;

    if (strncmp(line_cased, "EHLO ", 5) == 0)
    {
        mg_printf(nc, "%s", "250-localhost\n");
        mg_printf(nc, "%s %d\n", "250-SIZE ", mgos_sys_config_get_smtp_max_mail_size());
        // mg_printf(nc, "%s", "250 AUTH CRAM-MD5\n");
        bool plain = SMTP_AUTH_PLAIN_IS_ENABLED();
        bool login = SMTP_AUTH_LOGIN_IS_ENABLED();
        bool cram = SMTP_AUTH_CRAM_IS_ENABLED();
        if (plain || login || cram)
        {
            mg_printf(nc, "%s", "250 AUTH ");
            if (plain)
                mg_printf(nc, "%s", "PLAIN ");
            if (login)
                mg_printf(nc, "%s", "LOGIN ");
            if (cram)
                mg_printf(nc, "%s", "CRAM-MD5 ");
            mg_printf(nc, "%s", "\n");
        }
        return;
    }
    else if (strncmp(line_cased, "QUIT", 4) == 0)
    {
        mg_printf(nc, "%s", "221 localhost QUIT\n");
        nc->flags |= MG_F_SEND_AND_CLOSE;
        return;
    }
    else if (strncmp(line_cased, "RSET", 4) == 0)
    {
        mg_printf(nc, "%s", "250 Ok\n");
        clear_state(state);
        return;
    }
    else if (strncmp(line_cased, "NOOP", 4) == 0)
    {
        // LOG(LL_INFO, ("#################  NOOP"));
        mg_printf(nc, "%s", "250 Ok\n");
        return;
    }
    else
    {
        LOG(LL_INFO, ("### 500 unrecognized command <%s> %lu free", line, (unsigned long)mgos_get_free_heap_size()));
        mg_printf(nc, "%s", "500 unrecognized command\n");
        return;
    }

    mg_printf(nc, "%s", "221 localhost getline failure\n");
    nc->flags |= MG_F_SEND_AND_CLOSE;
}

static void ev_handler(struct mg_connection *nc, int ev, void *ev_data, void *ud)
{
    (void)nc;
    (void)ev_data;
    struct mbuf *io = &nc->recv_mbuf;
    mail_state *state = (mail_state *)nc->user_data;

    switch (ev)
    {
    case MG_EV_ACCEPT:
        state = new mail_state();
        state->s = new stringstream(); //(void *)++s_next_id;
        nc->user_data = (void *)state;
        //LOG(LL_INFO, ("#################  Connected"));
        mg_printf(nc, "%s", "220 localhost ESMTP smtp-server\n");
        break;
        // case MG_EV_HTTP_REQUEST: {
        //   struct work_request req = {(unsigned long)nc->user_data};

        //   if (write(sock[0], &req, sizeof(req)) < 0)
        //     perror("Writing worker sock");
        //   break;
        // }
    case MG_EV_RECV:
        //LOG(LL_INFO, ("#################  Recv"));
        // LOG(LL_INFO, ("#################  buffer %d", io->len));
        state->s->write(io->buf, io->len);
        char line[1000];
        do
        {
            state->s->getline(line, 990);
            int ln = strlen(line);
            if (ln >= 1 && line[ln - 1] == '\r')
            {
                line[ln - 1] = '\0';
            }
            if (ln > 1 || state->receiving_data)
            {
                // LOG(LL_INFO, ("#################  Line [%s]", line));
            }
            process_server_line(nc, line);
        } while (!state->s->fail());
        state->s->clear();
        // LOG(LL_INFO, ("#################  Sbuffer %d ## %d", state->s->str().length(), state->s->gcount()));
        state->s->str(state->s->str().substr(state->s->tellg())); // leave in stream only the rest
        // LOG(LL_INFO, ("#################  Sbuffer %d ## %d", state->s->str().length(), state->s->gcount()));
        mbuf_remove(io, io->len); // Discard data from recv buffer
        break;
    case MG_EV_CLOSE:
    {
        LOG(LL_DEBUG, ("#################  Smtp server Connection closed"));
        clear_state(state);
        state->s = NULL;
        state = NULL;
        if (nc->user_data)
            nc->user_data = NULL;
    }
    }
}

// void ev_handler(struct mg_connection *nc, int ev, void *ev_data, void *ud)
// {
//     struct mbuf *io = &nc->recv_mbuf;

//     switch (ev)
//     {
//     case MG_EV_RECV:
//         // This event handler implements simple TCP echo server
//         LOG(LL_INFO, ("Received: %x %x", make_word(io->buf[3], io->buf[2]), make_word(io->buf[1], io->buf[0])));
//         // if (make_word(io->buf[1], io->buf[0]) == 0x3452 && make_word(io->buf[3], io->buf[2]) == 0x6456 && millis() - click_req > 2000)
//         // {
//         //     click_req = millis();
//         // }
//         mbuf_remove(io, io->len); // Discard data from recv buffer
//         break;
//     default:
//         break;
//     }
//     (void)ev_data;
//     (void)ud;
// }

void setup_server()
{
    struct mg_mgr *mgr = mgos_get_mgr();
    char address[64];
    snprintf(address, sizeof(address), "tcp://:%d", mgos_sys_config_get_smtp_server_port());
    mg_bind(mgr, address, ev_handler, NULL);
}