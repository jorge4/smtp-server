#include "smtp_server_internal.h"

bool send_mail_process(struct mg_connection *nc, char *line, char *line_cased)
{
    mail_state *state = (mail_state *)nc->user_data;

    if (state->receiving_data)
    {
        string line_s(line);
        if (line_s == ".")
        {
            //data finished
            state->receiving_data = false;
            // TODO send mail
            LOG(LL_DEBUG, ("++++++++ new MAIL:  from <%s> data <%s>", state->from, state->mail_data->str().c_str()));
            // LOG(LL_INFO, ("++++++++  MAIL data: <%s> <%s>", state->mail_data->str().c_str(), state->from));
            char *error;
            smtp_mail curr;
            curr.from = string(state->from);
            curr.mail_data = state->mail_data->str();
            curr.user = string(state->user);
            //std::copy(state->to->begin(), state->to->end(), curr.to.begin());
            curr.to = *state->to;
            bool ret = mail_handler(&curr, &error, mail_ud);
            if (ret)
            {
                mg_printf(nc, "%s", "250 Ok: queued as 12345\n");
            }
            else
            {
                mg_printf(nc, "%s\n", error);
            }
            nc->flags |= MG_F_SEND_AND_CLOSE;

            // nc->flags |= MG_F_SEND_AND_CLOSE;
            // clear_state(state);
            // state->s = NULL;
            // state = NULL;
            // if (nc->user_data)
            //     nc->user_data = NULL;
        }
        else
        {
            int len = state->mail_data->str().length();
            if (line_s != "" || len > 0)
                *(state->mail_data) << line_s << "\r\n";
            else
            {
                //LOG(LL_INFO, ("++++++++  MAIL data: skipped empty"));
            }
            if (len + line_s.length() > mgos_sys_config_get_smtp_max_mail_size())
            {
                if (!mgos_sys_config_get_smtp_ignore_extra_data())
                {
                    mg_printf(nc, "%s", "552 message size exceeds fixed maximium message size\n");
                    //clear_state(state);
                    state->receiving_data = false;
                    state->mail_data->clear();
                }else{
                    state->data_overflow = true;
                    //LOG(LL_INFO, ("mdi_ov"));

                    // LOG(LL_INFO, ("++++++++  MAIL data: ignored overflow"));
                    // LOG(LL_INFO, ("++++++++  MAIL data: ignored ^#%s#^",line_s.c_str()));
                }
            }
        }
        return true;
    }
    else if (strncmp(line_cased, "MAIL FROM", 8) == 0)
    {
        if (mail_handler == NULL)
        {
            mg_printf(nc, "%s", "502 The command is not implemented\n");
            return true;
        }

        if (!state->autenticated && !SMTP_AUTH_ANON_IS_ENABLED())
            goto not_authed;
        //  LOG(LL_INFO, ("++++++++++++++  Send mail from: %s", line + 10));

        mg_printf(nc, "%s", "250 Ok\n");
        char *p = strchr(line + 10, '<') + 1;
        int pos = strchr(p, '>') - p;
        state->from = strdup(string(p).substr(0, pos).c_str());
        LOG(LL_DEBUG, ("++++++++++++++  Send mail from: %s", state->from));
        state->pending_mail = true;
        state->data_overflow = false;
        state->to = new vector<string>();
        return true;
    }
    else if (strncmp(line_cased, "RCPT TO", 7) == 0)
    {
        //LOG(LL_INFO, ("++++++++++++++  Send mail to: %s", line + 9));
        if (state->pending_mail)
        {
            char *p = strchr(line + 5, '<') + 1;
            int pos = strchr(p, '>') - p;

            string to = string(p).substr(0, pos);
            state->to->push_back(to);
            //LOG(LL_INFO, ("++++++++++++++  Send mail to: %s", to.c_str()));
            mg_printf(nc, "%s", "250 Ok\n");
        }
        else
        {
            mg_printf(nc, "%s", "503 Bad Sequence\n");
        }
        return true;
    }
    else if (strncmp(line_cased, "DATA", 4) == 0)
    {
        //LOG(LL_INFO, ("++++++++++++++  Receiving data "));
        state->receiving_data = true;
        state->mail_data = new stringstream();
        if (state->pending_mail)
        {
            if (state->to->empty())
            {
                state->to->clear();
                delete state->to;
                state->to = NULL;
                state->pending_mail = false;
                free(state->from);
                mg_printf(nc, "%s", "515 Destination mailbox address invalid\n");
            }
            else
            {
                mg_printf(nc, "%s", "354 End data with <CR><LF>.<CR><LF>\n");
            }
        }
        else
        {
            mg_printf(nc, "%s", "503 Bad Sequence\n");
        }
        return true;
    }

    return false;
not_authed:
    mg_printf(nc, "%s", "503 Not Logged In\n");
    return true;
}