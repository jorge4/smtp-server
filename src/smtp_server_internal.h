
#pragma once

#include "mgos.h"
#include "mgos_mongoose.h"
#include "smtp_server.h"
#include "mgos_sys_config.h"
#include "esp_system.h"

#ifdef __cplusplus
extern "C"
{
#endif

    void setup_server();
    bool mgos_smtp_server_init(void);
    double millis(void);

#ifdef __cplusplus
}
#endif


#define make_word(hi, lo) ((hi << 8) | lo)


#ifdef __cplusplus

#include <string>
#include <sstream>
#include <bits/stdc++.h>
#include <mbedtls/md.h>
#include <mbedtls/base64.h>
#include <vector>

using namespace std;

typedef struct
{
    stringstream *s;
    char *user;
    char *from;
    vector<string> *to;
    bool autenticated;
    bool waiting_plain;
    bool waiting_login_user;
    bool waiting_login_pass;
    char *cram_challenge;
    bool waiting_cram_response;
    bool receiving_data;
    bool data_overflow;
    stringstream *mail_data;
    bool pending_mail;
} mail_state;

void clear_state(mail_state *state);


extern smtp_mail_handler_function mail_handler;
#endif

extern smtp_auth_handler_function auth_handler;
extern void * auth_ud;
extern smtp_auth_cram_handler_function auth_cram_handler;
extern void * auth_cram_ud;
extern void * mail_ud;

#define SMTP_AUTH_PLAIN_IS_ENABLED() (mgos_sys_config_get_smtp_auth_plain_enable() && (auth_handler != NULL))
#define SMTP_AUTH_LOGIN_IS_ENABLED() (mgos_sys_config_get_smtp_auth_login_enable() && (auth_handler != NULL))
#define SMTP_AUTH_CRAM_IS_ENABLED() (mgos_sys_config_get_smtp_auth_cram_md5_enable() && (auth_cram_handler != NULL))
#define SMTP_AUTH_ANON_IS_ENABLED() (mgos_sys_config_get_smtp_auth_anonymous_enable())

// #define MAX_DATA_SIZE   2048

bool auth_process(struct mg_connection *nc, char *line, char *line_cased);
bool send_mail_process(struct mg_connection *nc, char *line, char *line_cased);
void process_server_line(struct mg_connection *nc, char *line);